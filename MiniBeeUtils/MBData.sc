MBData {
    classvar <>resamplingFreq = 20;
    classvar <>oscOutAddress;

    var <>minibeeID;
    var <>windowSize;
    var buffer;
    var <delta, <x, <y, <z, <xdir, <ydir, <movingAverage;
    var data, dataMul=15, dataOffset=7.0;
    var prevData;
    var runningSum=0;
    var task;
    var oscFunc;
    var <>sendOscOut = false;

    *new { arg minibeeID=10, windowSize=50;
        ^super.newCopyArgs(minibeeID, windowSize).init;
    }

    init {
        data = 0.0 ! 3;
        prevData = 0.0 ! 3;
        oscOutAddress = NetAddr("localhost", 5000);
        buffer = List.newClear(windowSize).fill(0);
        this.createOscFunc;
        this.createTask;
    }

    createOscFunc {
        oscFunc = OSCFunc({|oscdata|
            data = oscdata[2..] * dataMul - dataOffset;
            data = data.clip(0.0, 1.0);
            x = data[0];
            y = data[1];
            z = data[2];
        }, '/minibee/data', argTemplate: [minibeeID]);
    }

    sendOSC {
        if(sendOscOut){
            oscOutAddress.sendMsg("/mb/data", minibeeID, x, y, z, delta, movingAverage);
        }
    }

    createTask {
        task = TaskProxy.new({
            inf.do {
                this.calcXdir;
                this.calcYdir;
                this.calcDelta;
                this.calcMovingAverage;
                this.sendOSC;
                resamplingFreq.reciprocal.wait;
            }
        }).play;
    }

    calcDelta {
        delta = (data - prevData).abs.sum/3;
        prevData = data.copy;
        ^delta;
    }

    calcXdir {
        xdir = data[0] - prevData[0];
        if (xdir.isPositive){ 
            xdir = 1;
        }{
            xdir = -1;
        };
    }

    calcYdir {
        ydir = data[1] - prevData[1];
        if (ydir.isPositive){ 
            ydir = 1;
        }{
            ydir = -1;
        };
    }

    calcMovingAverage {
        buffer.addFirst(delta);
        runningSum = runningSum + buffer[0];
        runningSum = runningSum - buffer[buffer.size - 1];
        buffer.pop;
        movingAverage = runningSum / buffer.size;
    } 
} 
