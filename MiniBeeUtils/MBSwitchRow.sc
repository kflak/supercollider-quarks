MBSwitchRow {
    classvar <>launchpad;
    classvar <>mb = #[9, 10, 11, 12, 13, 14, 15, 16]; 

    var <row;
    var <mbDeltaTrigArray;
    var <fxChain;
    var <onColor;
    var <offColor;
    var <buttons; 
    var <chainSwitch;
    var <muted = true;

    *new {|row=0, mbDeltaTrigArray, fxChain, onColor=\redhi, offColor=\redlo|
        ^super.newCopyArgs(row, mbDeltaTrigArray, fxChain, onColor, offColor).init;
    }

    init {
        buttons = mb.collect({|id, idx|
            var addr = row * 16 + idx; 
            LPButton.new(
                addr: addr, 
                type: \note, 
                onFunc: { 
                    mbDeltaTrigArray[idx].play;
                    // if(muted){
                        // this.prPlayFxChain;
                        // chainSwitch.setNoteColor(addr, onColor);
                        // muted = false;
                    // }
                }, 
                offFunc: { mbDeltaTrigArray[idx].stop },
                toggle: true,
                onColor: onColor, 
                offColor: offColor,
            ) 
        });

        chainSwitch = LPButton.new(
            addr: (row * 16 + 8),
            type: \note,
            onFunc: { this.prPlayFxChain },
            offFunc: { this.prStopFxChain },
            toggle: true,
            onColor: onColor,
            offColor: offColor,
        )
    }

    prPlayFxChain {
        if (fxChain.isCollection){
            fxChain.do(_.play)
        }{
            fxChain.play;
        }
    }

    prStopFxChain {
        if (fxChain.isCollection){
            fxChain.do(_.stop);
        }{
            fxChain.stop;
        } 
    }

    prFreeFxChain {
        if (fxChain.isCollection){
            fxChain.do(_.free);
        }{
            fxChain.free;
        } 
    }

    free {
        buttons.do(_.free);
        chainSwitch.free;
        this.prFreeFxChain;
        mbDeltaTrigArray.do(_.free);
    } 
}
