MBSketch {
    classvar <>numSpeakers = 2;
    classvar <>server;
    classvar <>mb = #[9, 10, 11, 12, 13, 14, 15, 16]; 

    var <>fadeInTime,<>level, <>fadeOutTime, <>out;
    var <>speedlim, <>threshold, <>minibeeID, <>minAmp, <>maxAmp, <mbTrig;
    var <group, <instr, <fxGroup, <gainGroup;
    var <fxBus, <fx, <bus, gain;

    *new { |fadeInTime=1, level=1.0, fadeOutTime=1, out=0, speedlim=0.5, threshold=0.1, minibeeID=9, minAmp = -20, maxAmp=0, mbTrig|
        ^super.newCopyArgs(fadeInTime, level, fadeOutTime, out, speedlim, threshold, minibeeID, minAmp, maxAmp, mbTrig).init;
    }

    init {
        server = server ? Server.default;
        group = Group.new;
        fxGroup = Group.tail(group);
        gainGroup = Group.after(fxGroup);
        instr = Group.head(group);
        bus = List.new();
        fx = List.new();
        bus.add(Bus.audio(server, numSpeakers));
        fxBus = bus[0];
    }

    addFx {|... args|
        var p, name, params, inbus, outbus;
        name = args[0];
        if(args.size>1){
            params = args[1..];
        }{
            params = [];
        };
        bus.add(Bus.audio(server, numSpeakers));
        inbus = bus[bus.size-2];
        outbus = bus.last;
        p = params ++ [
            \in, inbus,
            \out, outbus,
        ];
        p = p.flat;
        fx.add(Synth.tail(fxGroup, name, p)); 
    }

    addParFx {|fxList|
        var inbus, outbus, fxSynths;
        bus.add(Bus.audio(server, numSpeakers));
        inbus = bus[bus.size-2];
        outbus = bus.last;
        fxSynths = fxList.collect({|i|
            var name, params, p;
            name = i[0];
            params = i[1..];
            p = params ++ [
                \in, inbus,
                \out, outbus,
            ];
            p = p.flat;
            Synth.tail(fxGroup, name, p); 
        });
        fx.add(fxSynths);
    }

    mbTrig_ {|function|
        if (minibeeID.isInteger){ 
            minibeeID = Array.newClear(1).fill(minibeeID);
        };
        mbTrig = mb.collect{|id|
            MBDeltaTrig.new(
                speedlim,
                threshold,
                id,
                minAmp,
                maxAmp,
                function,
            )
        };
    }

    prGain {
        var gainBus;
        gain = NodeProxy.new(server, \audio, numSpeakers);
        if(fx.isEmpty){ 
            gainBus = fxBus 
        }{
            gainBus = bus.last
        };
        gain.source = {In.ar(gainBus, numSpeakers)}; 
        gain.play(out, numSpeakers, gainGroup, vol: level, fadeTime: fadeInTime);
    }

    play {|i|
        this.prGain;
        case 
        {i.isInteger}{
            mbTrig[mb.indexOf(i)].play;
        }
        {i.isCollection}{
            i.do{|item|
                mbTrig[mb.indexOf(item)].play;
            }
        };
    } 

    stop {|i|
        case 
        {i.isInteger}{
            mbTrig[mb.indexOf(i)].stop;
        }
        {i.isCollection}{
            i.do{|item|
                mbTrig[mb.indexOf(item)].stop;
            }
        };
    }

    release {
        Routine ({
            gain.end(fadeOutTime);
            fadeOutTime.wait;
            this.free;
        }).play
    }

    free {
        Routine({
            mbTrig.do(_.free);
            5.wait;
            group.free;
            bus.do(_.free);
            fxBus.free;
        }).play;
    } 
}
