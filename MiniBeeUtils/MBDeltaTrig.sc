MBDeltaTrig {

    classvar <>mbData;
    classvar <>resamplingFreq = 20;
    classvar <>debug = false;

    var <>speedlim, <>threshold;
    var <>minibeeID;
    var <>minAmp, <>maxAmp;
    var <>function;
    var <>presetDict;
    var <>probability;
    var <>preset;
    var <task;
    var <deltaFunc;

    *new { arg speedlim=0.5, threshold=0.1, minibeeID=10, minAmp=0.0, maxAmp=0.3, function, presetDict, probability=1.0;
        ^super.newCopyArgs(speedlim, threshold, minibeeID, minAmp, maxAmp, function, presetDict, probability).init;
    }

    init {
    }

    set {|...args|
        if(args.size.even){
            args.pairsDo{|param, value|
                switch( param,
                    \threshold, { threshold = value },
                    \speedlim, { speedlim = value },
                    \minAmp, { minAmp = value },
                    \maxAmp, { maxAmp = value },
                )
            }
        }{
            "Odd number of arguments. Aborting".postln; 
        }
    }

    loadPreset{|p|
        if(presetDict.isNil){
            "Please set a preset dictionary first!".postln;
            "The dictionary should contain one or more dictionaries".postln;
            "containing parameters and values".postln;
        }{ 
            var argArray;
            preset = presetDict[p];
            argArray = preset.getPairs;
            this.set(*argArray);
        }
    }

    createTask {
        task = TaskProxy.new( {
            var free = true;
            inf.do({
                var dt = deltaFunc.delta;
                // var xdir = deltaFunc.xdir;
                // var ydir = deltaFunc.ydir;
                if(free) {
                    if((dt > threshold) && probability.coin){
                        function.value(dt, minAmp, maxAmp, preset);
                        if(debug){[dt, minibeeID].postln};
                        free = false;
                        SystemClock.sched(speedlim,{
                            free = true;
                        });
                    };
                };
                resamplingFreq.reciprocal.wait;
            });
        });
    }

    play { 
        deltaFunc = mbData[minibeeID];
        this.stop;
        this.createTask;
        task.play;
    }

    stop {
        task.stop;
        // task = nil;
    }

    free {
        task.stop;
        task = nil;
    }
}
