Push {
    var <element, <button, <midiout, <uid, <rgb, <led, <biLed, <>channel, <midifunc, <noteOns, <noteOffs, <ccs, <touches; 

    *new { 
        ^super.new.init();
    }

    init {
        element = (
            // encoders, cc
            \enc: (71..79),
            // biLed, cc
            \top1: (20..27),
            //rgb, cc
            \top2: (102..109),
            //rgb, note
            \pad: Array2D.fromArray(8, 8, (36..99).clump(8).reverse.flat),
            //led, cc, right-hand side buttons with time signatures
            \side: (43..36),
        );
        button = (
            // led, cc, all those other buttons
            \accent: 57,
            \addfx: 52,
            \addtrack: 53,
            \auto: 89,
            \back: 63,
            \browse: 111,
            \clip: 113,
            \delete: 118,
            \device: 110,
            \double: 117,
            \down: 47,
            \dupl: 88,
            \fix: 90,
            \forward: 62,
            \left: 44,
            \master: 28,
            \metro: 9,
            \mute: 60,
            \new: 87,
            \note: 50,
            \octavedown: 54,
            \octaveup: 55,
            \pan: 115, 
            \play: 85,
            \quant: 116,
            \rec: 86,
            \repeat: 56,
            \right: 45,
            \scales: 58,
            \select: 48,
            \session: 51,
            \shift: 49,
            \solo: 61,
            \stop: 29,
            \tap: 3,
            \track: 112,
            \undo: 119,
            \up: 46,
            \user: 59,
            \volume: 114,
        );
        rgb = (
            \off: 0,
            \white: 3,
            \redDim: 4,
            \red: 5,
            \orangeDim: 8,
            \orange: 9,
            \yellowDim: 12,
            \yellow: 13,
            \greenDim: 24,
            \green: 25,
            \blueDim: 40,
            \blue: 41,
        );
        biLed = (
            \off: 0,
            \redDim: 1, 
            \redDimBlink: 2,
            \redDimBlinkFast: 3,
            \red: 4,
            \redBlink: 5,
            \redBlinkFast: 6,
            \orangeDim: 7,
            \orangeDimBlink: 8,
            \orangeDimBlinkFast: 9,
            \orange: 10,
            \orangeBlink: 11,
            \orangeBlinkFast: 12,
            \limeDim: 13,
            \yellowDimBlink: 14,
            \yellowDimBlinkFast: 15,
            \lime: 16,
            \yellowBlink: 17,
            \yellowBlinkFast: 18,
            \greenDim: 19,
            \greenDimBlink: 20,
            \greenDimBlinkFast: 21,
            \green: 22,
            \greenBlink: 23,
            \greenBlinkFast: 24,
        );
        led = (
            \off: 0, 
            \dim: 1, 
            \dimBlink: 2, 
            \dimBlinkFast: 3, 
            \lit: 4, 
            \litBlink: 5, 
            \litBlinkFast: 6
        );
        channel = 0;
        noteOns = nil ! 128;
        noteOffs = nil ! 128;
        ccs = nil ! 128;
        touches = nil ! 128;
    }

    connect {
        if (MIDIClient.initialized){
            MIDIClient.sources.do{|device, idx|
                if(device.name.contains("Ableton Push MIDI 2")){
                    MIDIIn.connect(idx, device);
                    "% connected\n".postf(device.name);
                    midiout = MIDIOut.newByName("Ableton Push", "Ableton Push MIDI 2");
                    midiout.latency = 0;
                    uid = device.uid;
                }
            };
            if(uid.isNil){
                "Push not connected".postln;
            }
        }{
            MIDIClient.init;
            this.connect;
        }
    }

    setPad {| row, col, color=\red |
        var noteNum = element[\pad][row, col];
        midiout.noteOn(channel, noteNum, rgb[color]);
    }

    setAllPads {| color = \red |
        element[\pad].asArray.do{|i|
            midiout.noteOn(channel, i, rgb[color]);
        };
    } 

    setTop1 {| i, color=\red |
        var ccNum = element[\top1][i];
        midiout.control(channel, ccNum, rgb[color]);
    }

    setAllTop1 {| color = \red |
        element[\top1].do{|i|
            midiout.control(channel, i, biLed[color]); 
        }
    }

    setTop2 {| i, color=\red |
        var ccNum = element[\top2][i];
        midiout.control(channel, ccNum, rgb[color]);
    }

    setAllTop2 {| color = \red |
        element[\top2].do{|i|
            midiout.control(channel, i, rgb[color]); 
        } 
    }

    setSide {| i, color=\lit |
        var ccNum = element[\side][i];
        midiout.control(channel, ccNum, led[color]);
    }

    setAllSide {| color=\lit |
        element[\side].do{|ccNum|
            midiout.control(channel, ccNum, led[color]);
        } 
    }

    setBt {| name, color=\lit |
        var ccNum = button[name];
        midiout.control(channel, ccNum, led[color]);
    }

    setAllBt {| color=\lit |
        button.do{|ccNum|
            midiout.control(channel, ccNum, led[color]);
        } 
    }

    top1 {| i, function |
        var ccNum = element[\top1][i];
        if(uid.notNil){
            if(ccs[ccNum].notNil){ccs[ccNum].free};
            ccs[ccNum] = MIDIFunc.cc(function, ccNum, channel, uid);
        }{
            "Push not connected".postln
        }
    }

    top2 {| i, function |
        var ccNum = element[\top2][i];
        if(uid.notNil){
            if(ccs[ccNum].notNil){ccs[ccNum].free};
            ccs[ccNum] = MIDIFunc.cc(function, ccNum, channel, uid);
        }{
            "Push not connected".postln
        }
    }

    enc {| i, function |
        var ccNum = element[\enc][i];
        if(uid.notNil){
            if(ccs[ccNum].notNil){ccs[ccNum].free};
            ccs[ccNum] = MIDIFunc.cc(function, ccNum, channel, uid);
        }{
            "Push not connected".postln
        }
    }

    noteOn {| row, col, function |
        var noteNum = element[\pad][row, col];
        if(uid.notNil){
            if(noteOns[noteNum].notNil){noteOns[noteNum].free};
            noteOns[noteNum] = MIDIFunc.noteOn(function, noteNum, channel, uid);
        }{
            "Push not connected".postln
        }
    }

    noteOff {| row, col, function |
        var noteNum = element[\pad][row, col];
        if(uid.notNil){
            if(noteOffs[noteNum].notNil){noteOffs[noteNum].free};
            noteOffs[noteNum] = MIDIFunc.noteOff(function, noteNum, channel, uid);
        }{
            "Push not connected".postln
        } 
    }

    touch {| row, col, function |
        var noteNum = element[\pad][row, col];
        if(uid.notNil){
            touches[noteNum] = MIDIFunc.polytouch(function, noteNum, channel, uid);
        }{
            "Push not connected".postln
        }
    }

    sysex {| x, y, str |
        var msg, addr;
        addr = [(0, 17..67), (9, 26..67)].lace(8);
        x = addr[x];
        str = str.clump(8).at(0).padRight(8);
        msg = Int8Array[240, 71, 127, 21, 24 + y, 0, str.size + 1, x];
        str.do { |char| msg = msg.add(char.ascii) };
        msg.add(247);
        midiout.sysex(msg);
    }

    clearDisplay {
        4.do{|y|
            8.do{|x|
                this.sysex(x, y, "       ");
            }
        }
    }

    side {| i, function |
        var ccNum = element[\side][i];
        if(uid.notNil){
            if(ccs[ccNum].notNil){ccs[ccNum].free};
            ccs[ccNum] = MIDIFunc.cc(function, ccNum, channel, uid);
        }{
            "Push not connected".postln
        }
    }

    bt {| name, function |
        var ccNum = button[name];
        if(uid.notNil){
            if(ccs[ccNum].notNil){ccs[ccNum].free};
            ccs[ccNum] = MIDIFunc.cc(function, ccNum, channel, uid);
        }{
            "Push not connected".postln
        }
    }

    freeAllNoteOn {
        noteOns.do(_.free);
        noteOns = nil ! 128;
    }
    
    freeAllNoteOff {
        noteOffs.do(_.free);
        noteOffs = nil ! 128;
    }
    
    freeEnc { |i|
        var ccNum = element[\enc][i];
        ccs[ccNum].free;
        ccs[ccNum] = nil;
    }

    freeAllEnc {
        element[\enc].do{|i|
            var ccNum = i;
            ccs[ccNum].free;
        };
        ccs = nil ! 128;
    } 

    freeSide {|i|
        var ccNum = element[\side][i];
        ccs[ccNum].free;
        ccs[ccNum] = nil;
    }

    freeAllSide {
        element[\side].do{|i|
            var ccNum = i;
            ccs[ccNum].free;
            ccs[ccNum] = nil;
        }
    } 

    freeAllMidiFuncs {
        [noteOns, noteOffs, ccs, touches].do{|i| 
            i.do{|j|
                j.free
            }
        }
    }
}

