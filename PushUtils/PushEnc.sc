PushEnc {
    classvar <>push;
    var <>enc;
    var <currentValue;

    *new{|enc=0|
        ^super.newCopyArgs(enc).init;
    }

    init {
        this.checkForPush;
    }

    set {|synth, param=\amp, scale=1, min=0.0, max=1.0|
        var newValue;
        synth.get(param, {|pr| currentValue = pr});
        push.enc(enc, {|val| 
            if(val > 63){val = val - 128};
            val = val * scale;
            synth.get(param, {|pr| currentValue = pr});
            if(param == \amp){
                newValue = currentValue.ampdb + val;
                newValue = newValue.dbamp;
            }{
                newValue = currentValue + val;
            };
            newValue = newValue.clip(min, max);
            synth.set(param, newValue);
            push.sysex(enc, 1, param.asString);
            push.sysex(enc, 2, newValue.asString);
            // currentValue = newValue;
        });
    }

    checkForPush{
        if(push.isNil){
            "Please point me to a Push!".postln;
        }
    }

    // get{
    //     ^currentValue;
    // }

    free {
        push.freeEnc(enc);
    } 
}
