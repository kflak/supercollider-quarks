LaunchPad {
    var <midiout, <idx, <device; 
    var <uid;
    var <pad, <top, <side;
    var <topFunc, <sideFunc, <padFunc;

    *new {
        ^super.new.init();
    }

    init { 
        this.connect; 
        pad = Array2D.fromArray(8, 8, 
            (0..7)++
            (16..23)++
            (32..39)++
            (48..55)++
            (64..71)++
            (80..87)++
            (96..103)++
            (112..119),
        );
        side = [8, 24, 40, 56, 72, 88, 104, 120];
        top = (104..111);
        padFunc = nil ! pad.size;
        sideFunc = nil ! side.size;
        topFunc = nil ! top.size;
    } 

    addPad {
        arg row=0, col=0, onFunc, offFunc, toggle=false, onColor=\redhi, offColor=\redlo;
        var addr = pad[row, col];
        if (padFunc[addr].notNil){ padFunc[addr].free };
        padFunc[addr] = LPButton(addr, \note, onFunc, offFunc, toggle, onColor, offColor);
    }

    freePad {
        arg row, col;
        var addr = pad[row, col];
        padFunc[addr].free; 
    }

    addSide {
        arg addr=0, onFunc, offFunc, toggle=false, onColor=\amberhi, offColor=\amberlo;
        if (sideFunc[addr].notNil){ sideFunc[addr].free };
        sideFunc[addr] = LPButton(side[addr], \note, onFunc, offFunc, toggle, onColor, offColor);
    }

    freeSide {
        arg addr;
        sideFunc[addr].free;
    }

    addTop {
        arg addr=0, onFunc, offFunc, toggle=false, onColor=\greenhi, offColor=\greenlo;
        if (topFunc[addr].notNil){ topFunc[addr].free };
        topFunc[addr] = LPButton(top[addr], \cc, onFunc, offFunc, toggle, onColor, offColor);
    }

    freeTop {
        arg addr;
        topFunc[addr].free;
    }

    connect {
        if (MIDIClient.initialized){
            MIDIClient.sources.do{|d, i|
                if(d.name.contains("Launchpad MIDI 1")){
                    idx = i;
                    device = d;
                    MIDIIn.connect(idx, device);
                    "% connected\n".postf(device.name);
                    midiout = MIDIOut.newByName("Launchpad", "Launchpad MIDI 1");
                    midiout.latency = 0;
                    uid = device.uid;
                    LPButton.midiout = midiout;
                    LPButton.uid = uid; 
                    midiout.control(176, 0, 127);
                    SystemClock.sched(2, {
                        midiout.control(176, 0, 0);
                    });
                }
            };
            if(uid.isNil){
                "LaunchPad not connected".postln;
            }
        }{
            MIDIClient.init;
            this.connect;
        }
    }

    free {
        padFunc.do(_.free);
        topFunc.do(_.free);
        sideFunc.do(_.free);
        MIDIIn.disconnect(idx, device);
    }
} 
