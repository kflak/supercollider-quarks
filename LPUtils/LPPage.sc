LPPage {

    classvar <page, <focusedPage=0;
    classvar <addrPad, <addrTop, <addrSide;

    var <pageNumber=0;
    var <pad, <top, <side;
    var <focus;

    *initClass {
        page = List.new(0);
        addrPad = Array2D.fromArray(8, 8, 
            (0..7)++
            (16..23)++
            (32..39)++
            (48..55)++
            (64..71)++
            (80..87)++
            (96..103)++
            (112..119),
        );
        addrTop = (104..111); 
        addrSide = [8, 24, 40, 56, 72, 88, 104, 120];
    }

    *next {
        var nextPage = focusedPage + 1;
        if(nextPage < page.size){
            page[nextPage].focus = true;
        }
    }

    *prev {
        var prevPage = focusedPage - 1;
        if(focusedPage > 0){
            page[prevPage].focus = true;
        }
    }

    *new {
        ^super.new.init;
    }

    init { 
        pad = nil ! 128;
        top = nil ! 8;
        side = nil ! 8;
        page.add(this);
        pageNumber = page.indexOf(this);
        this.prFocusPage;
    }

    focus_{|val|
        if(val!=focus){
            focus = val;
            if(focus){
                this.prFocusPage;
                pad.do{|i| if(i.notNil){ i.focus = true }};
                top.do{|i| if(i.notNil){ i.focus = true }};
                side.do{|i| if(i.notNil){ i.focus = true }};
            }{
                pad.do{|i| if(i.notNil){ i.focus = false }};
                top.do{|i| if(i.notNil){ i.focus = false }};
                side.do{|i| if(i.notNil){ i.focus = false }};
            };
        } 
    }

    prFocusPage {
        if(focusedPage!=pageNumber){
            page[focusedPage].focus = false;
            focusedPage = pageNumber;
            focus = true;
        };
    }

    addTop {|index=0, onFunc, offFunc, toggle=false, onColor=\redhi, offColor=\redlo|
        var addr = addrTop[index];
        if (top[index].notNil){ top[index].free };
        top[index] = LPButton.new(addr, \cc, onFunc, offFunc, toggle, onColor, offColor);
    }

    freeTop {|index=0|
        top[index].free; 
    }

    focusTop {|index, val=true|
        top[index].focus = val;
    } 

    addSide {|index=0, onFunc, offFunc, toggle=false, onColor=\redhi, offColor=\redlo|
        var addr = addrSide[index];
        if (side[index].notNil){ side[index].free };
        side[index] = LPButton.new(addr, \note, onFunc, offFunc, toggle, onColor, offColor);
    }

    freeSide {|index=0|
        side[index].free; 
    }

    focusSide {|index, val=true|
        side[index].focus = val;
    } 

    activeSide {|index, val|
       side[index].active = val;
    }

    addPad {|row=0, col=0, onFunc, offFunc, toggle=false, onColor=\redhi, offColor=\redlo|
        var index = addrPad[row, col];
        if (pad[index].notNil){ pad[index].free };
        pad[index] = LPButton.new(index, \note, onFunc, offFunc, toggle, onColor, offColor);
    }

    freePad {|row, col|
        var index = addrPad[row, col];
        if (pad[index].notNil){ pad[index].free };
    }
    
    getPad {|row, col|
        var index = addrPad[row, col];
        ^pad[index]
    }

    activePad {|row, col, val|
        var index = addrPad[row, col];
        if (pad[index].notNil){ pad[index].active = val }
    }

    focusPad {|row, col, val=true|
        var index = addrPad[row, col];
        pad[index].focus = val;
    } 

    setTopColor {|index=0, color=\red|
        top[index].setControlColor(color);
    } 

    setSideColor {|index=0, color=\red|
        side[index].setNoteColor(color);
    } 

    setPadColor {|row, col, color=\red|
        var index = addrPad[row, col];
        pad[index].setNoteColor(color);
    } 

    free { 
        pad.do(_.free);
        top.do(_.free);
        side.do(_.free);
        page[pageNumber] = nil;
    }
} 
