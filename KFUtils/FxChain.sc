FxChain { 
    classvar <>numSpeakers=2; 
    classvar <>server;

    var <>fadeInTime, <level, <>fadeOutTime, <>in, <>out, <>group, <>hook, <>numInputBusChannels;
    var <fx, <bus, <gain;
    var fxGroup, gainGroup;
    var inArgPassed=false;
    var <tasks;
    var names;
    var <>presetDict;
    var <>lag=0.1;

    *new {|fadeInTime=1, level=1.0, fadeOutTime=1, in, out=0, group, hook, numInputBusChannels|
        ^super.newCopyArgs(fadeInTime, level, fadeOutTime, in, out, group, hook, numInputBusChannels).init;
    }

    init { 
        group = group ?? {Group.new};
        server = group.server;
        bus = List.new();
        fx = IdentityDictionary.new();
        names = IdentityDictionary.new();
        if(numInputBusChannels.isNil){
            numInputBusChannels = numSpeakers;
        };
        if(in.isNil){
            bus.add(Bus.audio(server, numInputBusChannels));
            in = bus[0];
        }{
            bus.add(Bus.new(index: in, numChannels: numInputBusChannels, server: server));
            inArgPassed = true;
        };
        tasks = List.new;
    } 

    prSetParams {|args|
        var synth = args[0];
        var task = Array.newClear(args.size - 1); 
        args[1..].do{|i, idx|
            var param = i[0];
            var val = i[1].asStream;
            var timePattern = i[2].asStream;
            tasks.add( TaskProxy({ 
                if((val.isInteger)||(val.isFloat)){
                    synth.set(param, val);
                }{
                    timePattern = timePattern ? 1;
                    val.do({|v|
                        synth.set(param, v);
                        timePattern.next.wait;
                    });
                };
            }));
        }
    }

    prParseArgs {|...args|
        var synth = args[0];
        var rawArgs = args[1];
        var parsedArgs = [synth];
        rawArgs.do{|i, idx| 
            if(i.class==Symbol){
                var size, currentArray;
                parsedArgs = parsedArgs.add(Array.newClear(1));
                size = parsedArgs.size;
                currentArray = parsedArgs[size-1];
                currentArray[0] = i;
                currentArray = currentArray.add(rawArgs[idx+1]); 
                if((rawArgs[idx+2].class!=Symbol)&&(rawArgs[idx+2].isNil.not)){
                    currentArray = currentArray.add(rawArgs[idx+2]);
                }
            };
        };
        ^parsedArgs;
    }

    prMakeName {|name|
        var n;
        if(names.includesKey(name)){
            n = (name++names[name]).asSymbol;
            names[name] = names[name] + 1;
        }{
            n = name.asSymbol;
            names[name] = 1;
        }
        ^n;
    }

    loadPreset {|preset|
        if(presetDict.isNil){
            "Please set a preset dictionary first!".postln;
            "The dictionary should contains one or more dictionaries".postln;
            "containing synth names and parameters in arrays".postln;
        }{ 
            tasks.do(_.stop);
            presetDict[preset].keysValuesDo{|synth, params|
                var p = params ++ [\lag, lag];
                fx[synth].set(*p);
                // patterns are not yet supported in presets
            //     var parsedArgs;
            //     [synth, params].postln;
            //     parsedArgs = this.prParseArgs(fx[synth], params);
            //     parsedArgs.postln;
            //     this.prSetParams(parsedArgs);
            }
        }
    }

    add {|... args|
        var synth, p, name, n, params, inbus, outbus, parsedArgs;
        name = args[0];
        bus.add(Bus.audio(server, numSpeakers));
        inbus = bus[bus.size-2];
        outbus = bus.last;
        params = args[1];
        n = this.prMakeName(name);
        p = [ \in, inbus, \out, outbus ];
        synth = Synth.tail(group, name, p);
        fx[n] = synth;
        parsedArgs = this.prParseArgs(fx[n], params);
        this.prSetParams(parsedArgs);
    }

    addPar {|... fxList|
        var inbus, outbus, synths;
        bus.add(Bus.audio(server, numSpeakers));
        inbus = bus[bus.size-2];
        outbus = bus.last;
        fxList.do({|i, idx|
            var synth;
            if (i.class==Symbol) {
                var name = i, n, params, p, parsedArgs;
                if (fxList[idx+1].isArray){
                    params = fxList[idx+1];
                };
                p = [ \in, inbus, \out, outbus] ++ params; 
                synth = Synth.tail(group, name, p); 
                n = this.prMakeName(name);
                fx[n] = synth;
                parsedArgs = this.prParseArgs(fx[n], params);
                this.prSetParams(parsedArgs);
            }; 
        });
    }

    level_ {|l|
        level = l;
        fx[\fxOut].set(\amp, level);
    }

    play {|quant=0|
        var gainBus;
        tasks.do(_.play);
        if(fx.isEmpty){ 
            gainBus = in 
        }{
            gainBus = bus.last
        };
        fx[\fxOut] = SynthDef(\FXOut, {
            var env = EnvGen.kr(Env.asr( 
                \attack.kr(fadeInTime), 1, 
                \release.kr(fadeOutTime)), 
                gate: \gate.kr(1), 
                doneAction: \da.kr(2)
            );
            var lag = \lag.kr(lag);
            var in = In.ar(\in.kr(gainBus), numSpeakers);
            var sig = in * \amp.kr(level, lag) * env;
            Out.ar(\out.kr(out), sig);
        }).play(target: group, addAction: \addToTail);
    }

    stop {
        tasks.do(_.stop);
        fx[\fxOut].set(\release, fadeOutTime);
        fx[\fxOut].set(\gate, 0);
    }

    free {
        fx[\fxOut].set(\release, fadeOutTime);
        fx[\fxOut].set(\gate, 0);
        SystemClock.sched(fadeOutTime, {
            tasks.do(_.stop);
            group.free;
            bus.do({|i, idx|
                if(inArgPassed.not){
                    i.free;
                }{
                    if(idx>0){
                        i.free;
                    }
                };
            });
            hook !? hook.();
        });
    } 
} 
