BufArray : Array {

    var <>pathName;
    var <>buf;
    var <>server;

    *new { arg pathName;
        ^super.newCopyArgs(pathName).init;
    }

    init {
        server = Server.default;
        buf = [];
        this.loadBufs(pathName, [".wav", ".aiff", ".snd"]);
        ^buf;
    }

    loadBufs { arg path, ext;
        var results = [];
        var extSet = ext !? { ext.collectAs(_.asSymbol, IdentitySet) };
        var search = { arg p;
            if (p.isFile) {
                if(extSet.notNil) {
                    extSet.findMatch(("." ++ p.extension).asSymbol) !? {
                        results = results.add(p.fullPath);
                    };
                } { results = results.add(p.fullPath); };
            } {
                // continue in subfolders
                p.entries.do { arg e; search.(e) };
            };
        };
        search.(PathName(path));
        server.makeBundle(nil, {
            results.do{ arg file;
                buf = buf.add(Buffer.readChannel(Server.default, file, channels: 0));
            }; 
        });
        postf("Added % soundfiles from %\n", results.size, pathName);
    }
}
